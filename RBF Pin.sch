EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8268 5827
encoding utf-8
Sheet 1 1
Title "RBF Pin Logic"
Date "2020-10-02"
Rev "v0.0"
Comp "Aristotle Space & Aeronautics Team (ASAT)"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Components:NSVMMBT2222AM3T5G Q?
U 1 1 5F772F98
P 1750 2800
F 0 "Q?" H 3091 2746 50  0000 L CNN
F 1 "NSVMMBT2222AM3T5G" H 3091 2655 50  0000 L CNN
F 2 "SOTFL40P120X55-3N" H 3100 2900 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/NSVMMBT2222AM3T5G.pdf" H 3100 2800 50  0001 L CNN
F 4 "ON SEMICONDUCTOR - NSVMMBT2222AM3T5G - TRANS, NPN, 40V, 0.6A, 150DEG C, 0.64W" H 3100 2700 50  0001 L CNN "Description"
F 5 "0.55" H 3100 2600 50  0001 L CNN "Height"
F 6 "863-NSVMMBT2222AM3T5" H 3100 2500 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NSVMMBT2222AM3T5G?qs=HXFqYaX1Q2wQ%2F%2FIZXmlALg%3D%3D" H 3100 2400 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 3100 2300 50  0001 L CNN "Manufacturer_Name"
F 9 "NSVMMBT2222AM3T5G" H 3100 2200 50  0001 L CNN "Manufacturer_Part_Number"
	1    1750 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F773F21
P 2500 2450
F 0 "R?" H 2570 2496 50  0000 L CNN
F 1 "1k" H 2570 2405 50  0000 L CNN
F 2 "" V 2430 2450 50  0001 C CNN
F 3 "~" H 2500 2450 50  0001 C CNN
	1    2500 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F77436D
P 1900 3150
F 0 "C?" H 2015 3196 50  0000 L CNN
F 1 "100u" H 2015 3105 50  0000 L CNN
F 2 "" H 1938 3000 50  0001 C CNN
F 3 "~" H 1900 3150 50  0001 C CNN
	1    1900 3150
	1    0    0    -1  
$EndComp
$Comp
L Components:SI2318CDS-T1-GE3 Q?
U 1 1 5F774E07
P 3200 2050
F 0 "Q?" V 3993 1950 50  0000 C CNN
F 1 "SI2318CDS-T1-GE3" V 3902 1950 50  0000 C CNN
F 2 "SOT95P237X112-3N" H 3850 2150 50  0001 L CNN
F 3 "" H 3850 2050 50  0001 L CNN
F 4 "Vishay SI2318CDS-T1-GE3 N-channel MOSFET Transistor, 5.6 A, 40 V, 3-Pin SOT-23" H 3850 1950 50  0001 L CNN "Description"
F 5 "1.12" H 3850 1850 50  0001 L CNN "Height"
F 6 "781-SI2318CDS-T1-GE3" H 3850 1750 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Vishay-Semiconductors/SI2318CDS-T1-GE3?qs=FfLXECYddQRc%2F6NWDH3T%252Bw%3D%3D" H 3850 1650 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 3850 1550 50  0001 L CNN "Manufacturer_Name"
F 9 "SI2318CDS-T1-GE3" H 3850 1450 50  0001 L CNN "Manufacturer_Part_Number"
	1    3200 2050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F775C76
P 3300 2050
F 0 "R?" H 3370 2096 50  0000 L CNN
F 1 "220" H 3370 2005 50  0000 L CNN
F 2 "" V 3230 2050 50  0001 C CNN
F 3 "~" H 3300 2050 50  0001 C CNN
	1    3300 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F775E43
P 2150 2900
F 0 "R?" V 1943 2900 50  0000 C CNN
F 1 "1k" V 2034 2900 50  0000 C CNN
F 2 "" V 2080 2900 50  0001 C CNN
F 3 "~" H 2150 2900 50  0001 C CNN
	1    2150 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F776013
P 3000 1850
F 0 "R?" H 3070 1896 50  0000 L CNN
F 1 "220" H 3070 1805 50  0000 L CNN
F 2 "" V 2930 1850 50  0001 C CNN
F 3 "~" H 3000 1850 50  0001 C CNN
	1    3000 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F77616E
P 3000 2450
F 0 "R?" H 3070 2496 50  0000 L CNN
F 1 "10k" H 3070 2405 50  0000 L CNN
F 2 "" V 2930 2450 50  0001 C CNN
F 3 "~" H 3000 2450 50  0001 C CNN
	1    3000 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F7783CB
P 2650 1850
F 0 "C?" H 2765 1896 50  0000 L CNN
F 1 "10u" H 2765 1805 50  0000 L CNN
F 2 "" H 2688 1700 50  0001 C CNN
F 3 "~" H 2650 1850 50  0001 C CNN
	1    2650 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2700 3000 2600
Wire Wire Line
	2650 2250 3000 2250
Connection ~ 3000 2250
Wire Wire Line
	3000 2250 3000 2300
Wire Wire Line
	3300 2200 3300 2250
Wire Wire Line
	3300 2250 3000 2250
Wire Wire Line
	3300 1900 3300 1800
Wire Wire Line
	2300 2900 2500 2900
Connection ~ 2500 2900
Wire Wire Line
	2500 2900 2700 2900
Wire Wire Line
	2000 2900 1900 2900
Wire Wire Line
	1900 2900 1900 3000
Wire Wire Line
	2500 1500 2650 1500
Wire Wire Line
	3000 2000 3000 2250
Wire Wire Line
	2650 2000 2650 2250
Wire Wire Line
	3000 1700 3000 1500
Connection ~ 3000 1500
Wire Wire Line
	3000 1500 3100 1500
Wire Wire Line
	2650 1700 2650 1500
Connection ~ 2650 1500
Wire Wire Line
	2650 1500 3000 1500
Wire Wire Line
	2500 2600 2500 2900
Wire Wire Line
	2500 2300 2500 1500
$Comp
L power:GND #PWR?
U 1 1 5F7815B6
P 3000 3150
F 0 "#PWR?" H 3000 2900 50  0001 C CNN
F 1 "GND" H 3005 2977 50  0000 C CNN
F 2 "" H 3000 3150 50  0001 C CNN
F 3 "" H 3000 3150 50  0001 C CNN
	1    3000 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F781928
P 1900 3350
F 0 "#PWR?" H 1900 3100 50  0001 C CNN
F 1 "GND" H 1905 3177 50  0000 C CNN
F 2 "" H 1900 3350 50  0001 C CNN
F 3 "" H 1900 3350 50  0001 C CNN
	1    1900 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3350 1900 3300
Wire Wire Line
	3000 3150 3000 3100
$Comp
L Components:NSVMMBT2222AM3T5G Q?
U 1 1 5F789757
P 4450 2800
F 0 "Q?" H 5791 2746 50  0000 L CNN
F 1 "NSVMMBT2222AM3T5G" H 5791 2655 50  0000 L CNN
F 2 "SOTFL40P120X55-3N" H 5800 2900 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/NSVMMBT2222AM3T5G.pdf" H 5800 2800 50  0001 L CNN
F 4 "ON SEMICONDUCTOR - NSVMMBT2222AM3T5G - TRANS, NPN, 40V, 0.6A, 150DEG C, 0.64W" H 5800 2700 50  0001 L CNN "Description"
F 5 "0.55" H 5800 2600 50  0001 L CNN "Height"
F 6 "863-NSVMMBT2222AM3T5" H 5800 2500 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NSVMMBT2222AM3T5G?qs=HXFqYaX1Q2wQ%2F%2FIZXmlALg%3D%3D" H 5800 2400 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 5800 2300 50  0001 L CNN "Manufacturer_Name"
F 9 "NSVMMBT2222AM3T5G" H 5800 2200 50  0001 L CNN "Manufacturer_Part_Number"
	1    4450 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F78975D
P 5200 2450
F 0 "R?" H 5270 2496 50  0000 L CNN
F 1 "1k" H 5270 2405 50  0000 L CNN
F 2 "" V 5130 2450 50  0001 C CNN
F 3 "~" H 5200 2450 50  0001 C CNN
	1    5200 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F789763
P 4600 3150
F 0 "C?" H 4715 3196 50  0000 L CNN
F 1 "100u" H 4715 3105 50  0000 L CNN
F 2 "" H 4638 3000 50  0001 C CNN
F 3 "~" H 4600 3150 50  0001 C CNN
	1    4600 3150
	1    0    0    -1  
$EndComp
$Comp
L Components:SI2318CDS-T1-GE3 Q?
U 1 1 5F78976F
P 5900 2050
F 0 "Q?" V 6693 1950 50  0000 C CNN
F 1 "SI2318CDS-T1-GE3" V 6602 1950 50  0000 C CNN
F 2 "SOT95P237X112-3N" H 6550 2150 50  0001 L CNN
F 3 "" H 6550 2050 50  0001 L CNN
F 4 "Vishay SI2318CDS-T1-GE3 N-channel MOSFET Transistor, 5.6 A, 40 V, 3-Pin SOT-23" H 6550 1950 50  0001 L CNN "Description"
F 5 "1.12" H 6550 1850 50  0001 L CNN "Height"
F 6 "781-SI2318CDS-T1-GE3" H 6550 1750 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Vishay-Semiconductors/SI2318CDS-T1-GE3?qs=FfLXECYddQRc%2F6NWDH3T%252Bw%3D%3D" H 6550 1650 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 6550 1550 50  0001 L CNN "Manufacturer_Name"
F 9 "SI2318CDS-T1-GE3" H 6550 1450 50  0001 L CNN "Manufacturer_Part_Number"
	1    5900 2050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F789775
P 6000 2050
F 0 "R?" H 6070 2096 50  0000 L CNN
F 1 "220" H 6070 2005 50  0000 L CNN
F 2 "" V 5930 2050 50  0001 C CNN
F 3 "~" H 6000 2050 50  0001 C CNN
	1    6000 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F78977B
P 4850 2900
F 0 "R?" V 4643 2900 50  0000 C CNN
F 1 "1k" V 4734 2900 50  0000 C CNN
F 2 "" V 4780 2900 50  0001 C CNN
F 3 "~" H 4850 2900 50  0001 C CNN
	1    4850 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F789781
P 5700 1850
F 0 "R?" H 5770 1896 50  0000 L CNN
F 1 "220" H 5770 1805 50  0000 L CNN
F 2 "" V 5630 1850 50  0001 C CNN
F 3 "~" H 5700 1850 50  0001 C CNN
	1    5700 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F789787
P 5700 2450
F 0 "R?" H 5770 2496 50  0000 L CNN
F 1 "10k" H 5770 2405 50  0000 L CNN
F 2 "" V 5630 2450 50  0001 C CNN
F 3 "~" H 5700 2450 50  0001 C CNN
	1    5700 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F78978D
P 5350 1850
F 0 "C?" H 5465 1896 50  0000 L CNN
F 1 "10u" H 5465 1805 50  0000 L CNN
F 2 "" H 5388 1700 50  0001 C CNN
F 3 "~" H 5350 1850 50  0001 C CNN
	1    5350 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2700 5700 2600
Wire Wire Line
	5350 2250 5700 2250
Connection ~ 5700 2250
Wire Wire Line
	5700 2250 5700 2300
Wire Wire Line
	6000 2200 6000 2250
Wire Wire Line
	6000 2250 5700 2250
Wire Wire Line
	6000 1900 6000 1800
Wire Wire Line
	5000 2900 5200 2900
Connection ~ 5200 2900
Wire Wire Line
	5200 2900 5400 2900
Wire Wire Line
	4700 2900 4600 2900
Wire Wire Line
	4600 2900 4600 3000
Wire Wire Line
	5200 1500 5350 1500
Wire Wire Line
	5700 2000 5700 2250
Wire Wire Line
	5350 2000 5350 2250
Wire Wire Line
	5700 1700 5700 1500
Connection ~ 5700 1500
Wire Wire Line
	5700 1500 5800 1500
Wire Wire Line
	5350 1700 5350 1500
Connection ~ 5350 1500
Wire Wire Line
	5350 1500 5700 1500
Wire Wire Line
	5200 2600 5200 2900
Wire Wire Line
	5200 2300 5200 1500
$Comp
L power:GND #PWR?
U 1 1 5F7897AA
P 5700 3150
F 0 "#PWR?" H 5700 2900 50  0001 C CNN
F 1 "GND" H 5705 2977 50  0000 C CNN
F 2 "" H 5700 3150 50  0001 C CNN
F 3 "" H 5700 3150 50  0001 C CNN
	1    5700 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7897B0
P 4600 3350
F 0 "#PWR?" H 4600 3100 50  0001 C CNN
F 1 "GND" H 4605 3177 50  0000 C CNN
F 2 "" H 4600 3350 50  0001 C CNN
F 3 "" H 4600 3350 50  0001 C CNN
	1    4600 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3350 4600 3300
Wire Wire Line
	5700 3150 5700 3100
Wire Wire Line
	4600 2900 4500 2900
Connection ~ 4600 2900
Wire Wire Line
	1900 2900 1800 2900
Connection ~ 1900 2900
Wire Wire Line
	3500 1500 3600 1500
Wire Wire Line
	6200 1500 6300 1500
Text GLabel 6300 1500 2    50   Output ~ 0
RBF_OUT
Text GLabel 3600 1500 2    50   Output ~ 0
RBF_OUT
Wire Wire Line
	2500 1500 2400 1500
Connection ~ 2500 1500
Wire Wire Line
	5200 1500 5100 1500
Connection ~ 5200 1500
Text GLabel 2400 1500 0    50   Input ~ 0
RBF_IN
Text GLabel 5100 1500 0    50   Input ~ 0
RBF_IN
Text GLabel 4500 2900 0    50   Input ~ 0
CON_NO
Text GLabel 1800 2900 0    50   Input ~ 0
CON_NO
$EndSCHEMATC
